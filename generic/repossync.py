# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#   Valerio Cosentino <valcos@bitergia.com>
#


import argparse
import copy
import json
import logging
import sys

import requests
import gitlab


GITLAB_URL = 'https://gitlab.com'

OPENDEV_API_URL = "https://opendev.org/api/v1"
GITHUB_API_URL = "https://api.github.com"

GIT = 'git'
GITHUB = 'github'
GERRIT = 'gerrit'
GIT_EXTENSION = '.git'

MASTER_BRANCH = 'master'
UPDATE_ACTION = 'update'

COMMIT_MSG = "{} sync by RepOSSync"
PROJECTS_JSON = "projects.json"

FILTER_RAW_REPO = '{} --filter-raw=data.project:{}'
OPENDEV_ORG_REPOS = "{}/orgs/{}/repos"
REPO_HTML_URL = 'html_url'


class GitLabFileManager:
    """GitLab file manager.

    This class allows to load, upload and commit the projects.json to
    GitLab using the library python-gitlab (version == 1.9.0). Furthermore,
    it provides additional functionalities to remove and add repositories
    to the projects.json
    """
    def __init__(self, token):
        self.token = token
        self.gl = gitlab.Gitlab(GITLAB_URL, private_token=self.token)

    def load_file(self, project_id, filename):
        """Load a file from GitLab.

        :param project_id: Gitlab project id
        :param filename: file name

        :return: file ("utf-8")
        """
        project = self.gl.projects.get(project_id)

        f = project.files.get(file_path=filename, ref=MASTER_BRANCH)
        return f.decode().decode("utf-8")

    def upload_file(self, project_id, filename, content, message):
        """Upload a file to GitLab.

        :param project_id: Gitlab project id
        :param filename: file name
        :param content: content
        :param message: commit message
        """
        project = self.gl.projects.get(project_id)
        data = {
            "branch": MASTER_BRANCH,
            "commit_message": message,
            "actions": [
                {
                    'action': UPDATE_ACTION,
                    'file_path': filename,
                    'content': content
                }
            ]
        }
        project.commits.create(data)

    def update_projects(self, project_file, project_name, repos):
        """Update the projects.json file with the new repositories.

        :param project_file: projects.json
        :param project_name: the name of the project to update in the projects.json
        :param repos: a list of repos of the following shape
            [
                {
                    'git': ..,
                    'github/gerrit': ...
                },
                ...
            ]
        """
        updated_project_file = copy.deepcopy(project_file)
        if project_name not in project_file:
            updated_project_file[project_name] = {}

        for repo in repos:
            for data_source in repo.keys():
                repo_in_data_source = repo[data_source]

                # if the data source is `git`, check that
                # (i) the repo ending with .git and (ii) the repo not ending in .git
                # don't exist in the projects.json. If this is the case, add the repo
                # ending with .git to the projects.json. This implies that the repos not
                # ending in .git will be kept, while new repos will be always added with
                # the .git extension. This logic is needed to avoid updating the projects.json
                # and re-collect the git raw data for the repos not ending with .git (note that
                # the re-collection would be required since for the same repo, the uuids differ
                # when it ends with/without .git)
                if data_source == GIT:
                    if ((repo_in_data_source
                         not in updated_project_file[project_name][data_source]) and
                            (repo_in_data_source + GIT_EXTENSION
                             not in updated_project_file[project_name][data_source])):
                        updated_project_file[project_name][data_source].append(repo_in_data_source + GIT_EXTENSION)
                else:
                    if repo_in_data_source not in updated_project_file[project_name][data_source]:
                        updated_project_file[project_name][data_source].append(repo[data_source])

        return updated_project_file


def fetch(url, headers=None):
    """Fetch the data from an URL

    :param url: target URL
    :param headers: request's headers
    """
    r = requests.get(url, headers=headers)
    r.raise_for_status()

    return r


def derive_filter_raw_repo(url, filter_raw_url):
    """Derive the filtered raw repo from an URL and the filter-raw URL

    :param url: repo URL
    :param filter_raw_url: filter-raw URL
    """
    repo = '/'.join(url.split('/')[-2:])
    filter_raw_repo = FILTER_RAW_REPO.format(filter_raw_url, repo)

    return filter_raw_repo


def fetch_gerrit_repos(org, filter_raw_url):
    """Fetch gerrit repos.

    :param org: organization
    :param filter_raw_url: Filter raw URL

    :return: a list of repos
    """
    def add_repos(r):
        repos = []

        for repo in r.json():
            if repo['fork']:
                continue

            repos.append(
                {
                    GIT: repo[REPO_HTML_URL],
                    GERRIT: derive_filter_raw_repo(repo[REPO_HTML_URL], filter_raw_url)
                }
            )

        return repos

    repos = []

    url = OPENDEV_ORG_REPOS.format(OPENDEV_API_URL, org)
    r = fetch(url)

    repos.extend(add_repos(r))

    return repos


def fetch_github_repos(org, token=None):
    """Fetch github repositories.

    :param org: github organization

    :return: a list of repos
    """
    def add_repos(r):
        repos = []

        for repo in r.json():
            repos.append(
                {
                    GIT: repo[REPO_HTML_URL],
                    GITHUB: repo[REPO_HTML_URL]
                }
            )

        return repos

    headers = {}
    if token:
        headers = {'Authorization': 'token ' + token}

    repos = []

    url = "{}/users/{}/repos".format(GITHUB_API_URL, org)
    page = 1
    last_page = 0

    url_page = url + "?page=" + str(page)
    r = fetch(url_page, headers=headers)

    repos.extend(add_repos(r))

    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)

    while page < last_page:
        page += 1
        url_page = url + "?page=" + str(page)
        r = fetch(url_page, headers=headers)

        repos.extend(add_repos(r))

    return repos


def check_args(data_source, filter_raw_url):
    """Check the args `data_source` and `filter_raw_url`

    :param data_source: data source to update
    :param filter_raw_url: filter raw URL
    """
    if not filter_raw_url and data_source == GERRIT:
        msg = "Gerrit requires the param `--filter-raw-url`"
        logging.error(msg)
        sys.exit(-1)

    if filter_raw_url and data_source == GITHUB:
        msg = "param `--filter-raw-url` not needed for GitHub"
        logging.warning(msg)


def sync_project(gitlab_token, gitlab_project_id, org_name, project_name,
                 data_source, filter_raw_url, github_token=None):
    """Sync the GitHub or Gerrit repos together with their corresponding Git
    repos in the projects.json.

    :param gitlab_token: GitLab token
    :param gitlab_project_id: GitLab project ID
    :param org_name: Org name (in GitHub or OpenDev)
    :param project_name: the name of the project to update in the projects.json
    :param data_source: the data source to update (Github or Gerrit)
    :param filter_raw_url: filter raw URL
    :param github_token: GitHub token, it can be `None`
    """
    # check the arguments
    check_args(data_source, filter_raw_url)

    # init gitlab file manager
    gl = GitLabFileManager(gitlab_token)

    # load projects.json
    projects_json_raw = gl.load_file(gitlab_project_id, PROJECTS_JSON)
    projects_json = json.loads(projects_json_raw)

    # fetch the upstream repos
    if data_source == GITHUB:
        repos = fetch_github_repos(org_name, token=github_token)
    else:
        repos = fetch_gerrit_repos(org_name, filter_raw_url)

    # update the projects.json
    updated_projects_json = gl.update_projects(projects_json, project_name, repos)
    updated_projects_json_raw = json.dumps(updated_projects_json, indent=4)

    # commit the changes
    commit_msg = COMMIT_MSG.format(org_name)
    gl.upload_file(gitlab_project_id, PROJECTS_JSON, updated_projects_json_raw, commit_msg)


def parse_args():
    """Parse command line arguments."""

    parser = argparse.ArgumentParser()
    parser.add_argument("--gitlab-token",
                        help="GitLab token")
    parser.add_argument("--github-token", default=None,
                        help="GitHub token")
    parser.add_argument("--gitlab-project-id",
                        help="GitLab project id")
    parser.add_argument("--org-name",
                        help="Organization to sync")
    parser.add_argument("--project-name",
                        help="Project name in the projects.json")
    parser.add_argument("--source", choices=[GERRIT, GITHUB],
                        help="Data source to sync")
    parser.add_argument("--filter-raw-url", default=None,
                        help="Filter-raw URL")

    return parser.parse_args()


if __name__ == '__main__':
    """Sync the GitHub or Gerrit repos together with
    their corresponding Git repos in the projects.json.

    This script retrieves the projects.json located in GitLab, and
    depending on the `source` (i.e., GitHub or Gerrit) and the `org_name`
    selected, it fetches the repos from the GitHub or OpenDev APIs. Then,
    it updates the projects.json with the new repos for the given `org_name`,
    and finally commits the changes to the GitLab repository hosting
    the projects.json with the commit message `COMMIT_MSG`.

    Example of executions:
        repossync
            --gitlab-token ...
            --gitlab-project-id ...
            --org-name starlingx
            --project-name StarlingX
            --filter-raw-url review.opendev.org
            --source gerrit

        repossync
            --gitlab-token ...
            --gitlab-project-id ...
            --org-name Bitergia
            --project-name Bitergia
            --source github
            --github-token ... (optional)
    """
    args = parse_args()

    github_token = args.github_token
    gitlab_token = args.gitlab_token
    gitlab_project_id = args.gitlab_project_id
    org_name = args.org_name
    project_name = args.project_name
    data_source = args.source
    filter_raw_url = args.filter_raw_url

    sync_project(gitlab_token, gitlab_project_id, org_name, project_name,
                 data_source, filter_raw_url, github_token=github_token)
