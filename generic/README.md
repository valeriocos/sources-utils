#get_channel_mattermost.py

#get_channel_slack.py

#githubsync.py
This script synchronizes the repositories of the projects.json (hosted in GitLab) with the GitHub repositories. 

GitLab: `https://gitlab.com/bitergia/c/<environment>/sources`.

You need to have a `gitlab token`, `gitlab project id` and `github organization` that you want to synchronize:
```
python3 githubsync.py <gitlab_token> <gitlab_project_id> <github_org>
```

If we want to execute it with many projects at once use the script `execute_githubsync.py` but you will need to have a JSON config file named `githubsync.json` in the same path as the script with the following format.
```
{
  "bitergia": {
    "project_id": "<bitergia_gitlab_project_id>",
    "github": [
      {
        "alias": "Bitergia",
        "name": "bitergia"
      },
      {
        "alias": "GrimoireLab",
        "name": "grimoirelab"
      }
    ]
  },
  "chaoss": {
    "project_id": "<chaoss_gitlab_project_id>",
    "github": [
      {
        "alias": "Chaoss",
        "name": "chaoss"
      }
    ]
  },
  "gitlab_token": "<TOKEN>"
}
```
